import numpy as np
from sklearn.datasets import load_boston
import matplotlib.pyplot as plt
from sklearn.preprocessing import scale


def data():
    X, y = load_boston(return_X_y=True)
    N = X.shape[0]
    X, y = scale(X), scale(y) # Scale data to stabilize training
    X = np.hstack([X, np.ones((N, 1))])
    inds = np.random.permutation(N)
    X_train, y_train = X[inds][:400], y[inds][:400]
    X_test, y_test = X[inds][400:], y[inds][400:]
    return X_train, y_train, X_test, y_test


def loss(x, y, w, lamb=0):
    return (np.square(x @ w - y)).mean() + (lamb * (w.T @ w))


def gradient(x, y, w, lamb=0):
    return 2 * (((x @ w) - y) @ x + 2 * lamb * w)


def stopping_rule(val_losses):
    a = np.array(val_losses)
    if len(val_losses) < 100: # Run at least 100 iterations
        return True
    if len(a) % 100 == 0:  # Hasn't improved for 100 iterations
        return np.any(a[-100:] > a[-1])
    return True


def step_size(eta, tau, grads, method="determ"):
    if method == "const":
        return eta
    if method == "determ":
        return eta / (1 + eta * tau * len(grads))
    if method == 'adagrad':
        g = np.array(grads)
        s_d = np.sum(np.square(grads), axis=0)
        return eta / (tau + np.sqrt(s_d))
    return 1e-3


def minibatch(X, y, batch_size=25):
    inds = np.random.permutation(X.shape[0])[:batch_size]
    return X[inds], y[inds]


def sgd(eta=1e-3, tau=.99, method="determ", lamb=.0, batch_size=25, verbose=False):
    w = np.zeros(14)
    val_losses = []
    train_losses = []
    grads = []

    it = 0
    while stopping_rule(val_losses):
        x, y = minibatch(X_train, y_train, batch_size=batch_size)
        
        grad = gradient(x, y, w, lamb=lamb)
        grads.append(grad)

        w -= step_size(eta, tau, grads, method) * grad

        val_losses.append(loss(X_test, y_test, w, lamb=0)) # Set lambda=0 for validation loss
        train_losses.append(loss(x, y, w, lamb=lamb))

        if verbose and it % 50 == 0: print("Iteration", it, "loss", val_losses[-1]) 

        it += 1

    return val_losses, train_losses, w


def gradient_descent(X_train, y_train, X_test, y_test):
    # These were found via grid search
    method = "adagrad"
    eta = 1
    tau = 0.2

    best_params = None
    best_loss = 1e10
    best_w = None
    for l in [0, 0.2]:
        for bs in [1, 25]:
            params = [eta, tau, method, l, bs]

            val_loss, train_loss, w = sgd(*params)
            loss_now = val_loss[-1]

            # Transform to number of samples seen
            val_loss = np.array(val_loss).reshape(-1, bs).mean(axis=1)
            train_loss = np.array(train_loss).reshape(-1, bs).mean(axis=1)

            param_str = r"$\lambda$: {}, batch_size: {}".format(l, bs)
            ax1.plot(range(bs, len(val_loss) * bs + 1, bs), val_loss, label="Validation loss " + param_str)
            ax2.plot(range(bs, len(val_loss) * bs + 1, bs), train_loss, label="Training loss " + param_str)

            if loss_now < best_loss:
                print("Best so far", params, loss_now)
                best_loss = loss_now
                best_params = params
                best_w = w

    return best_loss, best_params, best_w


if __name__ == '__main__':

    np.random.seed(1)

    print("Input shapes", [x.shape for x in data()])
    X_train, y_train, X_test, y_test = data()

    f, (ax1, ax2) = plt.subplots(1, 2)
    # ax1.set_yscale("log")
    # ax2.set_yscale("log")

    best_loss, best_params, best_w = gradient_descent(X_train, y_train, X_test, y_test)

    w_analytic = np.linalg.inv(X_train.T @ X_train) @ X_train.T @ y_train

    # ax1.plot([loss(X_test, y_test, w_analytic, lamb=0)] * 3500, label="Analytic solution")
    # Also plot analytic loss

    print("Best loss", best_loss)
    print("Best parameters", best_params)
    # plt.suptitle(r"Validation and training losses (log scale) for different hyperparameter settings."
    #              r" All run with adagrad, $\eta$ = 1, $\tau$ = 0.2")
    # ax1.set_xlabel("Number of samples seen")
    # ax2.set_xlabel("Number of samples seen")
    # ax1.set_ylabel("Loss (log scale)")
    # ax1.set_title("Validation loss")
    # ax2.set_title("Training loss")
    # ax1.legend()
    # ax2.legend()
    plt.savefig("losses.png", figsize=(20, 12), dpi=150)
    plt.clf()

    # Compare to analytic solution
    plt.yscale("linear")
    # plt.plot(np.sort(X_test @ w_analytic), label="Analytic solution")
    # plt.plot(np.sort(X_test @ best_w), label="Best parameters")
    # plt.plot(np.sort(y_test), label="Real")
    plt.title("Predictions on the validation set, sorted, scaled. Analytical solution for linear regression also included")
    plt.xlabel("Sample")
    plt.ylabel("Housing price")
    plt.legend()
    plt.savefig("predictions.png", figsize=(20, 12), dpi=150)