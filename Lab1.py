import numpy as np
import matplotlib.pyplot as plt

X = np.genfromtxt("ex_1_data.csv", delimiter=',')
N, D = X.shape

X = X.T

print(N)
print(D)

cov_X = np.cov(X)

eigenvals, eigenvecs = np.linalg.eig(cov_X)
idx = eigenvals.argsort()[::-1]
eigenvals = eigenvals[idx]
eigenvecs = eigenvecs[:,idx]
print(eigenvals)
print(eigenvecs.shape)

proj_X = eigenvecs[:, 0:2].T.dot(X)

plt.scatter(proj_X[0], proj_X[1])
plt.xlabel("x1 (largest eigenvalue)")
plt.ylabel("x2 (second largest eigenvalue)")
plt.title("Projection from 5-d space by PCA")
plt.show()


# ensure, that smallest eigenvecs correspond to less variance:

# proj_X = eigenvecs[:, -3:-1].T.dot(X)
# print(proj_X)
#
# plt.scatter(proj_X[0], proj_X[1])
# plt.show()

err = []

for L in range(1, D+1):
    proj_X = eigenvecs[:, 0:L].T.dot(X)

    proj_orig_space = eigenvecs[:, 0:L].dot(proj_X)

    diff = X - proj_orig_space
    diff_sq = (diff) ** 2
    sq__sum = (diff_sq).sum()
    err.append(sq__sum)

plt.plot(range(1, D+1), err, marker='o')
plt.xlabel("L")
plt.ylabel("Reconstruction error")
plt.title("Reconstruction error")
plt.show()

