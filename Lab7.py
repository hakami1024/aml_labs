import pickle

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
from sklearn.datasets import fetch_mldata
from sklearn.decomposition import PCA


def L(H, W, X):
    return ((X - W @ H) ** 2).sum()


def stopping_rule(val_losses):
    length = len(val_losses)
    if length < 100:
        return True
    if length % 100 == 0:  # Hasn't improved for 100 iterations
        return any([val_losses[-1] > v for v in val_losses[-100:-2]])
    return True


def nmf(X, K):
    N = X.shape[1]
    m = X.max()
    W = np.random.uniform(0, m, (X.shape[0], K))
    H = np.random.uniform(0, m, (K, N))

    err = 0.001

    losses = []

    while stopping_rule(losses):
        div_H = np.maximum(W.T @ W @ H, err)
        H = H * (W.T @ X) / div_H
        div_W = np.maximum(W @ H @ H.T, err)
        W = W * (X @ H.T) / div_W

        losses.append(L(H, W, X))

    return W, H, losses


def show_pkl(K):
    with open("results"+str(K)+".pkl", "rb") as f:
        W = pickle.load(f)
        H = pickle.load(f)

        # losses = pickle.load(f)
        # plt.plot(losses[K])
        # plt.show()
        #
        # return
        # outer = gridspec.GridSpec(1, 1, wspace=0.2, hspace=0.2)

        Ks = [K, ]  # 16, 64]
        dims = {8:[4, 2], 16: [4, 4], 64: [8, 8]}
        losses = {k: [] for k in Ks}
        for i, K in enumerate(Ks):
            # W, H, losses[K] = nmf(X, K)
            # print("Loss with K =", K, ":", L(H, W, X))

            fig, ax = plt.subplots(dims[K][0], dims[K][1])

            fig.suptitle("NMF basis vectors for K = "+str(K))

            for j in range(dims[K][0]):
                for k in range(dims[K][1]):
                    img = W[:, j * dims[K][1] + k].reshape((28, 28))
                    ax[j, k].imshow(img)
                    ax[j, k].set_xticks([])
                    ax[j, k].set_yticks([])

                    fig.add_subplot(ax[j, k])

            fig.subplots_adjust(top=0.5)
            fig.tight_layout()
            fig.show()


def main():
    mnist = fetch_mldata('MNIST original', data_home="/home/hakami/PycharmProjects/AML_Labs/")
    N = 200
    indices = np.random.choice(mnist.data.shape[0], N)
    X = mnist.data[indices].T
    fig = plt.figure(figsize=(16, 8))
    outer = gridspec.GridSpec(3, 1, wspace=0.2, hspace=0.2)
    Ks = [8, ]  # 16, 64]
    dims = [[4, 2], ]  # [4, 4], [8, 8]]
    losses = {k: [] for k in Ks}
    for i, K in enumerate(Ks):
        W, H, losses[K] = nmf(X, K)
        print("Loss with K =", K, ":", L(H, W, X))

        inner = gridspec.GridSpecFromSubplotSpec(dims[i][0], dims[i][1], subplot_spec=outer[i], wspace=0.1, hspace=0.1)

        for im_num in range(K):
            ax = plt.Subplot(fig, inner[im_num])
            ax.set_xticks([])
            ax.set_yticks([])
            fig.add_subplot(ax)
    # fig.show()
    fig.savefig("result.png")
    fig.clf()
    for K in Ks:
        plt.plot(losses[K])
        plt.show()


def pca(K):
    pca = PCA(n_components=K)
    with open("results"+str(K)+".pkl", "rb") as f:
        W = pickle.load(f)
        H = pickle.load(f)
        losses = pickle.load(f)
        X = pickle.load(f).T

        X_projected = pca.fit_transform(X)

        W = pca.components_
        print(W.shape)

        X_restored = pca.inverse_transform(X_projected)
        print(((X - X_restored) ** 2).sum())

        Ks = [K, ]  # 16, 64]
        dims = {8: [4, 2], 16: [4, 4], 64: [8, 8]}
        losses = {k: [] for k in Ks}
        for i, K in enumerate(Ks):
            # W, H, losses[K] = nmf(X, K)
            # print("Loss with K =", K, ":", L(H, W, X))

            fig, ax = plt.subplots(dims[K][0], dims[K][1])

            for j in range(dims[K][0]):
                for k in range(dims[K][1]):
                    img = W[j * dims[K][1] + k, :].reshape((28, 28))
                    ax[j, k].imshow(img)
                    ax[j, k].set_xticks([])
                    ax[j, k].set_yticks([])

                    fig.add_subplot(ax[j, k])
            fig.suptitle("PCA components for K = "+str(K))
            fig.show()
    # plt.plot(losses[8][93500:-1])


if __name__ == '__main__':
    # main()
    show_pkl(16)
    # pca(8)

