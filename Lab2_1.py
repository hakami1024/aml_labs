import numpy as np
import matplotlib.pyplot as plt


def L(alpha, x, y):
    return (y - alpha * x) ** 2


def estimate_risk(y, x, alpha, M):
    return 1/M*(L(alpha, x, y)).sum()

def ex_1_2():
    alpha0 = 0
    c = 2

    # estimating M

    alphas = np.arange(-5, 5.5, 0.5)

    for M in range(5, 100, 5):
        x = np.random.uniform(-c, c, M)
        y = alpha0 * x

        risk = []
        for alpha in alphas:
            risk.append(estimate_risk(y, x, alpha, M))

        plt.plot(alphas, risk)

    plt.plot(alphas, 4 / 3 * alphas ** 2, 'r--', linewidth=5)

    plt.xlabel("alpha")
    plt.ylabel("Risk")
    plt.title("Estimated risk with M in [5, 100]")

    plt.show()

    for M in range(500, 10000, 500):
        x = np.random.uniform(-c, c, M)
        y = alpha0 * x

        risk = []
        for alpha in alphas:
            risk.append(estimate_risk(y, x, alpha, M))

        plt.plot(alphas, risk)

    plt.plot(alphas, 4 / 3 * alphas ** 2, 'r--', linewidth=5)

    plt.xlabel("alpha")
    plt.ylabel("Risk")
    plt.title("Estimated risk with M in [500, 10000]")

    plt.show()

    risk_std = []
    risk_marg = []

    for M in range(50, 100000, 50):
        x = np.random.uniform(-c, c, M)
        y = alpha0 * x

        risk = []
        for alpha in alphas:
            risk.append(estimate_risk(y, x, alpha, M))

        risk_marg.append(risk[0])
        if len(risk_marg) > 100:
            risk_marg.pop(0)
        # plt.plot(alphas, risk)-

        risk_std.append(np.array(risk_marg).std())

    # plt.plot(alphas, 4 / 3 * alphas ** 2, 'r--')

    plt.plot(range(50, 100000, 50), risk_std)
    plt.xlabel("M")
    plt.ylabel("Variance in risk estimates")
    plt.title("Variance in risk estimates for alpha = -5")

    plt.show()

    M = 50000
    x = np.random.uniform(-c, c, M)
    y = alpha0 * x

    print(estimate_risk(y, x, -5, M))

    print(estimate_risk(y, x, -50, M))


def ex_1_4():
    alpha0 = 0
    c = 2

    M = 500000

    alphas = np.arange(-5, 5.5, 0.5)

    x = np.random.uniform(-c, c, M)
    y = alpha0 * x + np.random.normal()

    risk = []
    for alpha in alphas:
        risk.append(estimate_risk(y, x, alpha, M))

    plt.plot(alphas, risk)
    print(np.array(risk)[alphas==alpha0])
    print(min(risk))

    plt.xlabel("alpha")
    plt.ylabel("Risk")
    plt.title("Estimated risk with M = 500000")
    plt.show()

    # checking for different y-s:

    risks = []

    for i in range(1000):
        x = np.random.uniform(-c, c, M)
        y = alpha0 * x + np.random.normal()

        risk = []
        for alpha in alphas:
            risk.append(estimate_risk(y, x, alpha, M))

        plt.plot(alphas, risk)
        risks.append(risk)

    risks = np.array(risks)

    plt.plot(alphas, risks.mean(axis=0), 'r--', linewidth=5)

    plt.xlabel("alpha")
    plt.ylabel("Risk")
    plt.title("Estimated risk with M = 500000")
    plt.show()



if __name__ == '__main__':
    ex_1_2()
    ex_1_4()