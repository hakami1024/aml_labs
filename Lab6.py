import numpy as np
import matplotlib.pyplot as plt

from sklearn.datasets import load_boston
from sklearn.preprocessing import scale



def sigmoid(x):
    return 1 / (1 + np.exp(-x))

sigmoid.grad = lambda x: sigmoid(x)*(1-sigmoid(x))


def relu(x):
    return np.maximum(x, 0)

relu.grad = lambda x: np.where(x>0, 1, 0)


class MLP:

    def __init__(self, X, y, g):
        self.X = X
        self.y = np.reshape(y, newshape=(-1, 1))
        self.g = g

        inputDim = self.X.shape[1]

        self.V1 = np.random.normal(scale=1/(inputDim+8), size=(inputDim, 8))
        self.V2 = np.random.normal(scale=1/(8+4), size=(8, 4))
        self.V3 = np.random.normal(scale=1/(4+1), size=(4, 1))
        
        self.b1 = np.random.normal(scale=1/(inputDim+8), size=(1, 8))
        self.b2 = np.random.normal(scale=1/(8+4), size=(1, 4))
        self.b3 = np.random.normal(scale=1/(4+1))

    def __forward_step(self, X, V, b):
        a = X @ V + b
        return a, self.g(a)

    def __forward_pass(self, X):
        self.a1, self.z1 = self.__forward_step(X, self.V1, self.b1)
        # print(self.a1.shape, self.z1.shape)
        self.a2, self.z2 = self.__forward_step(self.a1, self.V2, self.b2)
        # print(self.a2.shape, self.z2.shape)
        y_hat, _ = self.__forward_step(self.a2, self.V3, self.b3)
        # print(self.a3.shape, y_hat.shape)

        return y_hat

    def __backward_pass(self, sigma, X):
        delta3 = sigma
        dV3 = (delta3.T @ self.z2).T
        # print("Shapes: ", delta3.shape, ", ", dV3.shape)

        delta2 = delta3 * self.V3.T * self.g.grad(self.a2)
        dV2 = self.z1.T @ delta2

        # print("Shapes: ", delta2.shape, ", ", dV2.shape)

        delta1 = delta2 @ self.V2.T * self.g.grad(self.a1)
        dV1 = (delta1.T @ X).T

        return dV1, delta1.sum(axis=0), dV2, delta2.sum(axis=0), dV3, delta3.sum(axis=0)

    def minibatch(self, X, y, batch_size=25):
        inds = np.random.permutation(X.shape[0])[:batch_size]
        return X[inds], y[inds]

    def stopping_rule(self, val_losses):
        a = np.array(val_losses)
        if len(val_losses) < 100:  # Run at least 100 iterations
            return True
        if len(a) % 100 == 0:  # Hasn't improved for 100 iterations
            return np.any(a[-100:] < a[-1])
        return True

    def step_size(self, eta, tau, grads, method="adagrad"):
        if method == "const":
            return eta
        if method == "determ":
            return eta / (1 + eta * tau * len(grads))
        if method == 'adagrad':
            s_d = np.sum(np.square(grads), axis=0)
            return eta / (tau + np.sqrt(s_d))
        return 1e-3

    def train(self, eta=1e-4, tau=.99, batch_size=100, verbose=True): # boilerplate version
        train_losses = []

        dV1s = []
        db1s = []
        dV2s = []
        db2s = []
        dV3s = []
        db3s = []

        it = 0
        while self.stopping_rule(train_losses):
            x, y = self.minibatch(self.X, self.y, batch_size=batch_size)

            y_hat = self.__forward_pass(x)
            dV1, db1, dV2, db2, dV3, db3 = self.__backward_pass(y_hat - y, x)

            dV1s.append(dV1)
            db1s.append(db1)
            dV2s.append(dV2)
            db2s.append(db2)
            dV3s.append(dV3)
            db3s.append(db3)

            self.V1 -= self.step_size(eta, tau, dV1s) * dV1
            self.b1 -= self.step_size(eta, tau, db1s) * db1
            self.V2 -= self.step_size(eta, tau, dV2s) * dV2
            self.b2 -= self.step_size(eta, tau, db2s) * db2
            self.V3 -= self.step_size(eta, tau, dV3s) * dV3
            self.b3 -= self.step_size(eta, tau, db3s) * db3

            train_losses.append(0.5*((y_hat - y)**2).sum())

            if verbose and it % 50 == 0: print("Iteration", it, "loss", train_losses[-1])

            it += 1

        return train_losses

    def loss(self, y, y_hat):
        return 0.5* (y - y_hat) ** 2

    def __forward_calc(self, X):
        a1, z1 = self.__forward_step(X, self.V1, self.b1)
        # print(self.a1.shape, self.z1.shape)
        a2, z2 = self.__forward_step(a1, self.V2, self.b2)
        # print(self.a2.shape, self.z2.shape)
        y_hat, _ = self.__forward_step(self.a2, self.V3, self.b3)
        # print(self.a3.shape, y_hat.shape)

        return y_hat
    # def __check_backward_step(self, x, y, V):
    #     dV = np.zeros(V.shape)
    # 
    #     V_hat = V.copy()
    # 
    #     # print(V_hat.shape)
    # 
    #     for i in range(dV.shape[0]):
    #         for j in range(dV.shape[1]):
    #             V_hat[i, j] =V_hat[i, j]+10**-5
    #             dV[i, j] = self.loss(x, y[:, j], V_hat)[:, j] - self.loss(x, y[:, j], V)[:, j]
    # 
    #     return dV
    #

    def __generate_V_hat(self, V):
        dV = np.zeros(V.shape)

        V_hat = V.copy()

        for i in range(dV.shape[0]):
            for j in range(dV.shape[1]):
                V_hat[i, j] =V_hat[i, j]+10**-5
                yield i, j, V_hat
                V_hat[i, j] = V[i, j]

    def _check_backward_pass(self):
        X = np.reshape(self.X[0, :], (1, -1))
        y_hat = self.__forward_pass(X)

        print("forward pass ended")

        y = self.y[0]
        dV1, db1, dV2, db2, dV3, db3 = self.__backward_pass(y_hat - y, X)

        dV_approx = np.zeros(shape=dV1.shape)

        for i, j, V_hat in self.__generate_V_hat(self.V1):
            a1, z1 = self.__forward_step(X, V_hat, self.b1)
            # print(self.a1.shape, self.z1.shape)
            a2, z2 = self.__forward_step(a1, self.V2, self.b2)
            # print(self.a2.shape, self.z2.shape)
            y_gen, _ = self.__forward_step(self.a2, self.V3, self.b3)

            dV_approx[i, j] = (self.loss(y_gen, y) - self.loss(y_hat, y))/10**-5




    # 
    #     eps = 10**-5
    # 
    #     dV3_hat = self.__check_backward_step(self.a2, y, self.V3)
    #     print(np.linalg.norm(dV3 - dV3_hat))
    # 
    #     dV2_hat = self.__check_backward_step(self.a1, self.a2, self.V2)
    #     print(np.linalg.norm(dV2 - dV2_hat))
    # 
    #     d1 = 0.5 * X ** 2 * eps
    #     d2 = self.loss(X, self.a1, self.V1) * X
    #     print("dV1_hat elements: ", d1.shape, d2.shape)
    #     dV1_hat = d1 - d2
    #     print(np.linalg.norm(dV1 - dV1_hat))




def data():
    X, y = load_boston(return_X_y=True)
    N = X.shape[0]
    X, y = scale(X), scale(y) # Scale data to stabilize training
    # X = np.hstack([X, np.ones((N, 1))])
    inds = np.random.permutation(N)
    X_train, y_train = X[inds][:400], y[inds][:400]
    X_test, y_test = X[inds][400:], y[inds][400:]
    return X_train, y_train, X_test, y_test


def main():
    X_train, y_train, X_test, y_test = data()
    mlp = MLP(X_train, y_train, relu)
    # mlp.__forward_pass()
    loss = mlp.train()

    plt.plot(loss)
    plt.title("Training loss")
    plt.show()

if __name__ == '__main__':
    main()