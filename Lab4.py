from sklearn.decomposition import PCA
from sklearn.datasets import fetch_mldata
from sklearn.manifold import TSNE
from sklearn.metrics.pairwise import euclidean_distances

import numpy as np
import matplotlib.pyplot as plt

import time


def neighbourhoods(X, sigma):
    D2 = euclidean_distances(X, squared=True) / (2 * sigma ** 2)

    for i in range(X.shape[0]):
        D2[i, i] = np.Inf

    return np.exp(-D2) / np.sum(np.exp(-D2), 0)


def loss(p, q):
    L = p*np.log(p/q)
    np.fill_diagonal(L, 0)
    return L.sum()


def gradient(p, q, z):
    grad = np.zeros(z.shape)

    PQdiff = p-q+p.T-q.T

    for i in range(z.shape[0]):
        z_diff = (z[i] - z)
        v = np.array([PQdiff[i], PQdiff[i]]).T
        grad[i] = 2 * (z_diff * v).sum(0)
        # for j in range(z.shape[0]):
        #     if i!= j:
        #         grad[i] += 2 * (z[i] - z[j]) * PQdiff[i, j]

    return grad


def stopping_rule(val_losses):
    a = np.array(val_losses)
    if len(val_losses) < 100:  # Run at least 100 iterations
        return True
    if len(a) % 100 == 0:  # Hasn't improved for 100 iterations
        return np.any(a[-100:] < a[-1])
    return True


def sgd(p, q, z, step, verbose=False):
    losses = []

    it = 0
    while stopping_rule(losses):
        grad = gradient(p, q, z)

        z -= step * grad
        q = neighbourhoods(z, 1)

        L = loss(p, q)
        losses.append(L)

        if verbose and it % 50 == 0: print("Iteration", it, "loss", losses[-1])

        it += 1

    return losses, z


if __name__ == '__main__':
    mnist = fetch_mldata('MNIST original', data_home="/home/hakami/PycharmProjects/AML_Labs/")
    indices = np.random.choice(mnist.data.shape[0], 2000)
    X = mnist.data[indices]


    print(X)

    print(X.shape)

    print("reducing dimensionality")

    X_transformed = PCA(n_components=50).fit_transform(X)

    z = np.random.normal(0, 0.1, X_transformed[:, 0:2].shape)
    print(z.shape)

    P = neighbourhoods(X_transformed, 1000)
    Q = neighbourhoods(z, 1)

    print(P.shape, Q.shape)

    print("start training")

    loss, z = sgd(P, Q, z, step=0.25, verbose=True)

    plt.plot(loss)
    plt.title("Training loss")
    plt.show()

    plt.scatter(z[:, 0], z[:, 1], c=mnist.target[indices], cmap=plt.cm.rainbow)
    plt.title("SNE result")
    plt.show()

    X_pca = PCA(n_components=2).fit_transform(X)
    plt.scatter(X_pca[:, 0], X_pca[:, 1], c=mnist.target[indices], cmap=plt.cm.rainbow)
    plt.title("PCA result")
    plt.show()

    z_sklearn = TSNE().fit_transform(X_transformed)

    plt.scatter(z_sklearn[:, 0], z_sklearn[:, 1], c=mnist.target[indices], cmap=plt.cm.rainbow)
    plt.title("scikit-TSNE result")
    plt.show()





