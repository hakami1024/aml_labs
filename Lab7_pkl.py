import numpy as np
from sklearn.datasets import fetch_mldata

import pickle


def L(H, W, X):
    return ((X - W @ H) ** 2).sum()


def stopping_rule(val_losses):
    length = len(val_losses)
    if length < 100: # Run at least 100 iterations
        return True
    if length % 100 == 0:  # Hasn't improved for 100 iterations
        return any([v > val_losses[-1] for v in val_losses[-100:-2]])
    if length > 10000:
    	return False
    return True


def nmf(X, K):
    N = X.shape[1]
    m = X.max()
    W = np.random.uniform(0, m, (X.shape[0], K))
    H = np.random.uniform(0, m, (K, N))

    err = 0.001

    losses = []

    while stopping_rule(losses):
        div_H = np.maximum(W.T @ W @ H, err)
        H = H * (W.T @ X) / div_H
        div_W = np.maximum(W @ H @ H.T, err)
        W = W * (X @ H.T) / div_W

        losses.append(L(H, W, X))

    return W, H, losses


if __name__ == '__main__':
    mnist = fetch_mldata('MNIST original', data_home="/home/khakimov/labs/")
    N = 400
    indices = np.random.choice(mnist.data.shape[0], N)
    X = mnist.data[indices].T

    Ks = [8]

    losses = {k:[] for k in Ks}
    for i, K in enumerate(Ks):
        W, H, losses[K] = nmf(X, K)
        print("Loss with K =", K, ":", L(H, W, X))

        with open("results" + str(K) + ".pkl", "wb+") as f:
            pickle.dump(W, f)
            pickle.dump(H, f)
            pickle.dump(losses, f)
            pickle.dump(X, f)