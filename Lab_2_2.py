from sklearn.datasets import load_boston
from sklearn.model_selection import train_test_split
import numpy as np

import matplotlib.pyplot as plt

class SGD():

    def __init__(self):
        self.theta = None

    def _compute_loss(self, X, y, lambdaval):
        D, N = X.shape
        return 1 / N * ((self.theta.T.dot(X) - y) ** 2).sum() + lambdaval * self.theta.T.dot(self.theta)[0, 0]

    def _compute_grad(self, X, y):
        D, N = X.shape
        return 2 / N * (self.theta.T.dot(X) - y).dot(X.T).T + 2 * self.lambda_val * self.theta

    def _compute_deterministic_step(self, iteration):
        return self.eta_0/(1+self.eta_0*self.tau*iteration)

    def _compute_adagrad_step(self, grad):
        if not hasattr(self, "grads"):
            self.grads = np.array(grad)
        else:
            self.grads = np.append(self.grads, grad, axis=1)

        s = (self.grads**2).sum(axis=1)
        return np.array([self.eta_0/(self.tau + np.sqrt(s))]).T

    def train(self,
              X,
              y,
              batch_size = 1,
              step_kind = "deterministic",
              eta_0 = 0.1,
              tau = 0.1,
              lambda_val = 0.01,
              epsilon = 10e-4,
              print_loss = False):

        D, N = X.T.shape
        D = D + 1

        ones = np.ones((N,1))
        X = np.append(X, ones, axis=1)

        X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=400)

        X_train = X_train.T
        X_test = X_test.T

        idx = np.arange(0, 400)
        np.random.shuffle(idx)

        self.validation_losses = [np.inf]
        self.training_losses = []

        self.validation_errors = []
        self.training_errors = []

        self.theta = np.random.rand(D, 1)
        self.lambda_val = lambda_val

        if step_kind == "deterministic":
            self._compute_step = self._compute_deterministic_step
            self.eta_0 = eta_0
            self.tau = tau
        elif step_kind == "adagrad":
            self._compute_step = self._compute_adagrad_step
            self.eta_0 = eta_0
            self.tau = tau
        else:
            raise Exception("invalid step_kind")

        i = 0
        pos = 0

        while True:

            if (pos+1)*batch_size >= 400: #new epoch
                idx = np.arange(0, 400)
                np.random.shuffle(idx)
                pos=0

                loss = self._compute_loss(X_test, y_test, self.lambda_val)

                if print_loss:
                    print("%d) Loss = %f" % (i, loss))

                if self.validation_losses[-1] - loss < epsilon:
                    break

                self.validation_losses.append(loss)
                self.validation_errors.append(self._compute_loss(X_test, y_test, 0))

            batch = idx[pos*batch_size:(pos+1)*batch_size]
            loss = self._compute_loss(X_train[:, batch], y_train[batch], self.lambda_val)
            self.training_losses.append(loss)
            self.training_errors.append(self._compute_loss(X_train[:, batch], y_train[batch], 0))

            grad = self._compute_grad(X_train[: ,batch], y_train[batch])

            if step_kind == "deterministic":
                step_arg = pos
            elif step_kind == "adagrad":
                step_arg = grad

            step = self._compute_step(step_arg)

            if step_kind == "deterministic":
                grad = grad/np.linalg.norm(grad)

            last_theta = self.theta
            step_val = step * grad
            self.theta = self.theta - step_val

            i = i+1
            pos = pos+1

        self.validation_losses.pop(0)

        self.theta = last_theta

    def predict(self, X):
        X = X.T
        _, N = X.shape

        ones = np.ones((1, N))
        X = np.append(X, ones, axis=0)
        y = self.theta.T.dot(X)[0]

        return y


if __name__ == '__main__':
    X, y = load_boston(return_X_y=True)

    print(X.shape)
    print(len(y))

    sgd = SGD()

    # sgd.train(X, y, batch_size=25, step_kind="adagrad", lambda_val=0, eta_0=0.2, tau=10e-6, print_loss=True)

    # sgd.train(X, y, batch_size=25, step_kind="adagrad", lambda_val=0.3, eta_0=0.01, tau=10e-6, print_loss=True)

    if False:
        min_err = float("inf")
        best = None
        for eta in [0.0001, 0.0005, 0.001, 0.005, 0.01, 0.05, 0.1, 0.5, 1]:
            for tau in [0.0001, 0.0005, 0.001, 0.005, 0.01, 0.05, 0.1, 0.5, 1]:
                sgd.train(X, y, batch_size=25, step_kind="deterministic", lambda_val=0.0, eta_0=eta,
                          tau=tau, print_loss=True)
                if sgd.validation_errors[-1] < min_err:
                    min_err = sgd.validation_errors[-1]
                    best = (sgd.training_errors, sgd.validation_errors, eta, tau)

        print(min_err)

        print(best[2])
        print(best[3])

        plt.plot(best[0], 'g')
        plt.xlabel("Iteration")
        plt.ylabel("Loss")
        plt.title("Convergence plot, training errors")

        plt.show()

        plt.plot(best[1], 'b')
        plt.xlabel("Epoch")
        plt.ylabel("Loss")
        plt.title("Convergence plot, validation errors")

        plt.show()

    if False:
        min_err = float("inf")
        best = None
        for eta in [0.0001, 0.0005, 0.001, 0.005, 0.01, 0.05, 0.1, 0.5, 1]:
            for tau in [0.0001, 0.0005, 0.001, 0.005, 0.01, 0.05, 0.1, 0.5, 1]:
                for lambdaval in [0.01, 0.1, 1]:
                    sgd.train(X, y, batch_size=25, step_kind="deterministic", lambda_val=lambdaval,
                              eta_0=eta, tau=tau, print_loss=True)
                    if sgd.validation_errors[-1] < min_err:
                        min_err = sgd.validation_errors[-1]
                        best = (sgd.training_errors, sgd.validation_errors, eta, tau, lambdaval)


        print(min_err)

        print(best[2])
        print(best[3])
        print(best[4])

        plt.plot(best[0], 'g')
        plt.xlabel("Iteration")
        plt.ylabel("Loss")
        plt.title("Convergence plot, training errors")

        plt.show()

        plt.plot(best[1], 'b')
        plt.xlabel("Epoch")
        plt.ylabel("Loss")
        plt.title("Convergence plot, validation errors")

        plt.show()

    if False:
        min_err = float("inf")
        best = None
        for eta in [0.01, 0.1]:
            sgd.train(X, y, batch_size=1, step_kind="adagrad", lambda_val=0.0, eta_0=eta,
                      tau=10e-5, print_loss=True)
            if sgd.validation_errors[-1] < min_err:
                min_err = sgd.validation_errors[-1]
                best = (sgd.training_errors, sgd.validation_errors, eta)

        print(min_err)

        print(best[2])

        plt.plot(best[0], 'g')
        plt.xlabel("Iteration")
        plt.ylabel("Loss")
        plt.title("Convergence plot, training errors")

        plt.show()

        plt.plot(best[1], 'b')
        plt.xlabel("Epoch")
        plt.ylabel("Loss")
        plt.title("Convergence plot, validation errors")

        plt.show()

    if True:
        min_err = float("inf")
        best = None
        for eta in [0.01]:
            for lambdaval in [0.1]:
                sgd.train(X, y, batch_size=25, step_kind="adagrad", lambda_val=lambdaval, eta_0=eta,
                          tau=0.000001, print_loss=True)
                if sgd.validation_errors[-1] < min_err:
                    min_err = sgd.validation_errors[-1]
                    best = (sgd.training_errors, sgd.validation_errors, eta, 10e-5, lambdaval)

        print(min_err)

        print(best[2])
        print(best[3])
        print(best[4])

        plt.plot(best[0], 'g')
        plt.xlabel("Iteration")
        plt.ylabel("Loss")
        plt.title("Convergence plot, training errors")

        plt.show()

        plt.plot(best[1], 'b')
        plt.xlabel("Epoch")
        plt.ylabel("Loss")
        plt.title("Convergence plot, validation errors")

        plt.show()


