import numpy as np
import matplotlib.pyplot as plt

from sklearn.metrics.pairwise import euclidean_distances
from sklearn.cluster import KMeans
from sklearn.neighbors import NearestNeighbors


def eigens(D, W, X):
    L = D - W
    eigenvals, eigenvecs = np.linalg.eig(L)

    idx = eigenvals.argsort()
    eigenvals = eigenvals[idx]
    eigenvecs = eigenvecs[:, idx]

    M = 8

    for (vec, col) in zip(eigenvecs[:, 0:M].T, ['r', 'g', 'b', 'm']):
        plt.plot(vec, col)

    plt.title("4 smallest eigenvectors")

    plt.show()

    Y = eigenvecs[:, 0:M]

    print(Y.shape)

    fig, axes = plt.subplots(M, M)

    for i in range(M):
         for j in range(i+1, M):
             axes[i, j].plot(Y[:, i], Y[:, j], '.')
    plt.title("4-D data representation")

    plt.show()

    Y_labels = KMeans(n_clusters=2).fit_predict(Y)

    idx1 = Y_labels == 0
    idx2 = Y_labels == 1

    fig, axes = plt.subplots(M, M)

    for i in range(M):
        for j in range(i + 1, M):
            axes[i, j].plot(Y[idx1, i], Y[idx1, j], 'r.')
            axes[i, j].plot(Y[idx2, i], Y[idx2, j], 'b.')
    plt.title("4-D data representation")

    plt.show()

    plt.plot(X[idx1, 0], X[idx1, 1], '.r')
    plt.plot(X[idx2, 0], X[idx2, 1], '.b')
    plt.title("Final clusterization")

    plt.show()


if __name__ == '__main__':
    X = np.genfromtxt("ex_3_data.csv", delimiter=',')
    plt.plot(X[:, 0], X[:, 1], '.k')
    plt.show()

    N = X.shape[0]

    d = euclidean_distances(X)

    X_labels = KMeans(n_clusters=2).fit_predict(X)

    idx1 = X_labels == 0
    idx2 = X_labels == 1

    plt.plot(X[idx1, 0], X[idx1, 1], '.r')
    plt.plot(X[idx2, 0], X[idx2, 1], '.b')
    plt.title("Initial K-means clusterization")
    plt.show()

    e = 0.5

    W1 = d <= e
    W1 = W1.astype(int)
    np.fill_diagonal(W1, 0)

    A = 8

    nearest = NearestNeighbors(n_neighbors=A, algorithm='brute', metric='euclidean').fit(X).kneighbors(return_distance=False)
    W2 = np.zeros(W1.shape)

    for i in range(N):
        for j in nearest[i]:
            W2[i, j] = 1

    D1 = np.zeros(W1.shape)
    D2 = np.zeros(W1.shape)

    D1[np.diag_indices(N)] = np.sum(W1 > 0, axis=1)
    np.fill_diagonal(D2, A)

    eigens(D1, W1, X)
    eigens(D2, W2, X)

